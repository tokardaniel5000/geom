package hu.Spatial.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import hu.Spatial.example.dto.SpatialDto;
import hu.Spatial.example.service.SpatialService;

@RestController
public class AppController {

	@Autowired
	private SpatialService spatialService;
	
	@GetMapping("/")
	public String sayHello() {
		return "Hello World!";
	}
	
	@GetMapping("/geoms")
	public List<SpatialDto> geoms() {
		return spatialService.findAll();
	}
}
