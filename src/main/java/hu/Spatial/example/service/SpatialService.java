package hu.Spatial.example.service;

import java.util.List;

import hu.Spatial.example.dto.SpatialDto;

public interface SpatialService {

	List<SpatialDto> findAll();
}
