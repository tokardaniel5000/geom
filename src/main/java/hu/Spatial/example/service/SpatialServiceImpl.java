package hu.Spatial.example.service;

import java.util.ArrayList;
import java.util.List;

import org.geotools.geometry.jts.WKBReader;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTWriter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.Spatial.example.dto.SpatialDto;
import hu.Spatial.example.entity.SpatialEntity;
import hu.Spatial.example.repository.SpatialRepository;

@Service
public class SpatialServiceImpl implements SpatialService {

	@Autowired
	private SpatialRepository spatialRepository;

	public List<SpatialDto> findAll() {
		
		ModelMapper modelMapper = new ModelMapper();

		List<SpatialEntity> resultEntity = spatialRepository.findAll();
		List<SpatialDto> resultDto = new ArrayList<>();
		
		for (SpatialEntity spatialEntity : resultEntity) {
			SpatialDto spatialDto = modelMapper.map(spatialEntity, SpatialDto.class);
			
			Geometry geom = readWkb(spatialEntity.getGeom());
			String wkt = getWktFromGeom(geom);

			spatialDto.setName(spatialEntity.getName());
			spatialDto.setArea(geom.getArea());
			spatialDto.setWkt(wkt);

			
			resultDto.add(spatialDto);
		}
		
		return resultDto;
	}

	private String getWktFromGeom(Geometry geometry) {
		
		WKTWriter wktWriter = new WKTWriter();
		
		return wktWriter.write(geometry);
	}
	
	private Geometry readWkb(byte[] bytes) {
		
		WKBReader wkbReader = new WKBReader();
		
		Geometry geom = null;
		
		try {
			geom = wkbReader.read(bytes);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return geom;
	}

}
