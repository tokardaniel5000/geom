package hu.Spatial.example.dto;

import lombok.Data;

@Data
public class SpatialDto {

	private String name;
	
	private byte[] geom;
	
	private Double area;
	
	private String wkt;
}
