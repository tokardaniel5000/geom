package hu.Spatial.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.Spatial.example.entity.SpatialEntity;


public interface SpatialRepository extends JpaRepository<SpatialEntity, Long>{
}
