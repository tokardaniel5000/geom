package hu.Spatial.example.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "spatial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SpatialEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	private String name;
	
	private byte[] geom;
}
